const express = require('express');

//Definimos la ruta donde encontrar los controladores usando la libreria Path
var path = require("path");
var ctrlDir = path.resolve("controllers/");

//Importamos el controllador
var userCtrl = require(path.join(ctrlDir, "chat"));
var router = express.Router();

//Middleware para mostrar datos del request
router.use (function (req,res,next) {
  console.log('/' + req.method);
  next();
});

//Analiza la ruta y el protocolo y reacciona en consecuencia
router.get('/',function(req,res){
  res.sendFile(path.resolve('views/index.html'));
});

router.get('/chat/list',function(req,res){
  res.sendFile(path.resolve('views/list.html'));
});

router.get('/chat/view',function(req,res){
  res.sendFile(path.resolve('views/view.html'));
});



module.exports = router;