document.addEventListener("DOMContentLoaded", function (event) {
    //Nos conectamos al servicio de socket.
    //La constante socket será nuestro vinculo con el servidor donde definiremos todas las accioens
    const socket = io("http://localhost:2000");
  
    /*
    ** Acciones que se realizan cuando se detecta el evento "connected" lanzado por el servidor
    */
    socket.on("connected", (data) => {
      console.log(data.msg);
    });  
}