const socketPort =  2000;
const { Server } = require("socket.io");
const socketServer = require("http").createServer();

socketServer.listen(socketPort, (err, res) => {
  if (err) console.log(`ERROR: Connecting APP ${err}`);
  else console.log(`Server is running on port ${socketPort}`);
});
const io = new Server(socketServer, {
  cors: {
    origin: "http://localhost:3000", //Esta será la dirección de vuestra web
  },
});

io.on("connection", (socket) => {
  console.log("Nuevo cliente conectado");
  socket.emit("connected", {
    msg: "Bienvenido al chat",
  });
});